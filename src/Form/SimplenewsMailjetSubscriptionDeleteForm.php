<?php

namespace Drupal\simplenews_mailjet_subscriptions\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the Simplenews Mailjet Subscriptions delete form.
 */
class SimplenewsMailjetSubscriptionDeleteForm extends EntityConfirmFormBase {

  /**
   * Return the question shown when deleting an entity.
   *
   * @return mixed
   *   Message shown when deleting a Simplenews Mailjet Subscription entity.
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %name?', ['%name' => $this->entity->getTitle()]);
  }

  /**
   * Get url used when canceling an entity deletion.
   *
   * @return \Drupal\Core\Url
   *   Cancellation URL.
   */
  public function getCancelUrl() {
    return new Url('entity.simplenews_mailjet_subscriptions.list');
  }

  /**
   * Get confirmation text.
   *
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * Get submission form when deleting a Simplenews Mailjet Subscription entity.
   *
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->delete();
    $this->messenger()->addMessage($this->t('Entity %label has been deleted.', ['%label' => $this->entity->getTitle()]));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
