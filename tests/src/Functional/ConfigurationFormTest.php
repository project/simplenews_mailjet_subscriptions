<?php

namespace Drupal\Tests\simplenews_mailjet_subscriptions\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test module.
 *
 * @group simplenews_mailjet_subscriptions
 */
class ConfigurationFormTest extends BrowserTestBase {

  protected static $modules = [
    'simplenews_mailjet_subscriptions',
  ];

  protected $defaultTheme = 'seven';

  /**
   * Set up the tests.
   */
  protected function setUp(): void {
    parent::setUp();

    $account =
            $this->drupalCreateUser([
              'administer site configuration',
            ]);

    $this->drupalLogin($account);
  }

  /**
   * Test if break site.
   */
  public function testModuleDoesNotBreaksSite() {
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Test if can access page.
   */
  public function testCanAccessContentMessages() {
    $this->drupalGet('/admin/config/services/simplenews/mailjet-subscriptions');
    $this->assertSession()->pageTextContains('Simplenews Maijet Subscription');
  }

}
