<?php

namespace Drupal\simplenews_mailjet_subscriptions;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an Simplenews Mailjet Subscription entity.
 */
interface SimplenewsMailjetSubscriptionsInterface extends ConfigEntityInterface {

  /**
   * Returns the entity title.
   *
   * @return string
   *   The entity title.
   */
  public function getTitle();

  /**
   * Sets the entity title.
   *
   * @param string $title
   *   Node title.
   *
   * @return $this
   *   The Simplenews Mailjet Subscription entity.
   */
  public function setTitle($title);

  /**
   * Returns the entity id.
   *
   * @return string
   *   The entity id.
   */
  public function getId();

  /**
   * Returns the entity api key.
   *
   * @return string
   *   The entity api key.
   */
  public function getApiKey();

  /**
   * Sets the entity api key.
   *
   * @param string $api_key
   *   Api key.
   *
   * @return $this
   *   The Simplenews Mailjet Subscription entity.
   */
  public function setApiKey($api_key);

  /**
   * Returns the entity secret key.
   *
   * @return string
   *   The entity secret key.
   */
  public function getSecretkey();

  /**
   * Sets the entity secret key.
   *
   * @param string $secret_key
   *   Secret key.
   *
   * @return $this
   *   The Simplenews Mailjet Subscription entity.
   */
  public function setSecretkey($secret_key);

  /**
   * Sets the entity mapping table.
   *
   * @param string $mapping_table
   *   Mapping table.
   *
   * @return $this
   *   The Simplenews Mailjet Subscription entity.
   */
  public function setMappingTable($mapping_table);

  /**
   * Returns the entity mapping table.
   *
   * @return string
   *   The entity mapping table.
   */
  public function getMappingTable();

  /**
   * Show a message accordingly to status, after creating/updating an entity.
   *
   * @param int $status
   *   Status int, returned after creating/updating an entity.
   */
  public function statusMessage($status);

}
