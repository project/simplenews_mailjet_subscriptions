<?php

namespace Drupal\simplenews_mailjet_subscriptions;

/**
 * Provides a list of Simplenews Mailjet Subscription entities.
 */
class SimplenewsMailjetSubscriptionsUtilities {

  /**
   * Check Subscriber.
   */
  private static function checkSubscriber($api_key, $secret_key, $encoded_mail) {

    // Check if contact exist
    // verifica se o contacto ja tem opções.
    $endpoint = 'https://api.mailjet.com/v3/REST/contactdata/' . $encoded_mail;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, $api_key . ':' . $secret_key);
    curl_setopt($ch, CURLOPT_URL, $endpoint);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($ch);
    curl_close($ch);
    $json = json_decode($response, TRUE);

    return $json;
  }

  /**
   * Update Subscriber.
   */
  private static function updateSubscriber($api_key, $secret_key, $listID, $subscriber_email, $contact_property, $newsletter_property_name, $action) {

    // Add contact to mailjet.
    $endpoint = 'https://api.mailjet.com/v3/REST/contactslist/' . $listID . '/managecontact';
    $message = [
      "Properties" => [
        $contact_property => $newsletter_property_name,
      ],
      "Action" => $action,
      "Email" => $subscriber_email,
    ];
    $payload = json_encode($message);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, $api_key . ':' . $secret_key);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($ch, CURLOPT_URL, $endpoint);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($ch);
    curl_close($ch);
    $json = json_decode($response, TRUE);

    return $json;
  }

  /**
   * Subscribe User.
   */
  public static function subscribeUser($subscriber, $subscription, $category, $api_key, $secret_key, $listID) {

    $contact_property = $category['contact_property'];
    $newsletter_property_name = $category['newsletter_property_name'];
    $subscriber_email = $subscriber->getMail();
    $encoded_mail = urlencode($subscriber_email);
    $response = '';

    $contact_info = self::checkSubscriber($api_key, $secret_key, $encoded_mail);

    $options = $contact_info['Data'];

    // Se existe - acrescenta a opções.
    if ($options[0]['Data']) {
      foreach ($options[0]['Data'] as $property) {
        if ($property["Name"] == $contact_property) {

          if (is_int(strpos($property['Value'], $newsletter_property_name))) {
            $newsletter_property_name = $property['Value'];
          }
          else {
            $newsletter_property_name = $property['Value'] . ';' . $newsletter_property_name;
          }
        }
      }
    }

    $response = self::updateSubscriber($api_key, $secret_key, $listID, $subscriber_email, $contact_property, $newsletter_property_name, 'addnoforce');
    $contact_id = $response['Data'][0]['ContactID'];
    \Drupal::logger('simplenews_mailjet_subscriptions')->notice('The mailjet user ' . $contact_id . ' subscribed to the ' . $contact_property . ' properties with the ' . $newsletter_property_name . ' values in the list ' . $listID . '.');
    return $response;
  }

  /**
   * Unsubscribe User.
   */
  public static function unsubscribeUser($subscriber, $subscription, $category, $api_key, $secret_key, $listID) {
    $contact_property = $category['contact_property'];
    $newsletter_property_name = $category['newsletter_property_name'];
    $subscriber_email = $subscriber->getMail();
    $encoded_mail = urlencode($subscriber_email);
    $response = '';

    $contact_info = self::checkSubscriber($api_key, $secret_key, $encoded_mail);
    $contact_id = $contact_info['ContactID'];
    $options = $contact_info['Data'];

    if ($options[0]['Data']) {
      foreach ($options[0]['Data'] as $property) {
        if ($property["Name"] == $contact_property) {
          if (substr_count($property['Value'], ';') == 0) {
            $newsletter_property_name = '';
            $response = self::updateSubscriber($api_key, $secret_key, $listID, $subscriber_email, $contact_property, $newsletter_property_name, 'remove');
            $contact_id = $response['Data'][0]['ContactID'];
            \Drupal::logger('simplenews_mailjet_subscriptions')->notice('The mailjet user ' . $contact_id . ' unsubscribed to the list ' . $listID . '.');
          }
          else {
            $pos = strpos($property['Value'], $newsletter_property_name);
            \Drupal::logger('simplenews_mailjet_subscriptions')->notice('The mailjet user ' . $contact_id . ' unsubscribed to the ' . $contact_property . ' properties with the ' . $newsletter_property_name . ' values in the list ' . $listID . '.');
            if ($pos === 0) {
              $newsletter_property_name = str_replace($newsletter_property_name . ';', '', $property['Value']);
            }
            else {
              $newsletter_property_name = str_replace(';' . $newsletter_property_name, '', $property['Value']);
            }
            $response = self::updateSubscriber($api_key, $secret_key, $listID, $subscriber_email, $contact_property, $newsletter_property_name, 'addnoforce');
            $contact_id = $response['Data'][0]['ContactID'];
            \Drupal::logger('simplenews_mailjet_subscriptions')->notice('The mailjet user ' . $contact_id . 'remains subscribed to the ' . $contact_property . ' properties with the ' . $newsletter_property_name . ' values in the list ' . $listID . '.');

          }
        }
      }
    }

    return $response;
  }

}
