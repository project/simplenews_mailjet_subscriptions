Simplenews Mailjet Subscriptions
---------------------

 INTRODUCTION
------------

The Simplenews Mailjet Subscriptions module allows for a simple subscription
integration between Simplenews module and Mailjet. 
You can subscribe users to your Mailjet list and associate 
them with a respective property

REQUIREMENTS
------------

This module requires the following modules:

 * Simplenews (https://www.drupal.org/project/simplenews)

 INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

 CONFIGURATION
-------------

 * You need to add some Simplenews Newsletter first
 * Go to /admin/config/services/simplenews/mailjet-subscriptions and 
 * add a new Simplenews Maijet Subscription configuration.
 * You can choose the Newsletter and map the property of the Mailjet list
 * (e.g. Category) with the value associated with your Newsletter.
