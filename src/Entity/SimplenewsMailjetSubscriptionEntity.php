<?php

namespace Drupal\simplenews_mailjet_subscriptions\Entity;

use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\simplenews_mailjet_subscriptions\SimplenewsMailjetSubscriptionsInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Simplenews Mailjet Subscription entity.
 *
 * @ConfigEntityType(
 *   id = "simplenews_mailjet_subscription",
 *   label = @Translation("Simplenews Mailjet Subscription"),
 *   handlers = {
 *    "list_builder" = "Drupal\simplenews_mailjet_subscriptions\Controller\SimplenewsMailjetSubscriptionsListBuilder",
 *    "form" = {
 *       "add" = "Drupal\simplenews_mailjet_subscriptions\Form\SimplenewsMailjetSubscriptionAddForm",
 *       "edit" = "Drupal\simplenews_mailjet_subscriptions\Form\SimplenewsMailjetSubscriptionEditForm",
 *       "delete" = "Drupal\simplenews_mailjet_subscriptions\Form\SimplenewsMailjetSubscriptionDeleteForm",
 *      },
 *    },
 *   config_prefix = "simplenews_mailjet_subsciption",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "title" = "title",
 *   },
 *   config_export = {
 *     "id",
 *     "title",
 *     "api_key",
 *     "secret_key",
 *     "mapping_table"
 *   }
 * )
 */
class SimplenewsMailjetSubscriptionEntity extends ConfigEntityBase implements SimplenewsMailjetSubscriptionsInterface {

  use StringTranslationTrait, MessengerTrait;

  /**
   * Simplenews Mailjet Subscription entity id.
   *
   * @var string
   */
  protected $id;

  /**
   * Simplenews Mailjet Subscription entity title.
   *
   * @var string
   */
  protected $title;

  /**
   * Node title.
   *
   * @var string
   */

  /**
   * Returns the entity title.
   *
   * @return string
   *   The entity title.
   */
  public function getTitle() {
    return $this->get('title');
  }

  /**
   * Sets the entity title.
   *
   * @param string $title
   *   Node title.
   *
   * @return $this
   *   The Simplenews Mailjet Subscription entity.
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * Returns the entity id.
   *
   * @return string
   *   The entity id.
   */
  public function getId() {
    return $this->get('id');
  }

  /**
   * Returns the entity api key.
   *
   * @return string
   *   The entity api key.
   */
  public function getApiKey() {
    return $this->get('api_key');
  }

  /**
   * Sets the entity api key.
   *
   * @param string $api_key
   *   Api key.
   *
   * @return $this
   *   The Simplenews Mailjet Subscription entity.
   */
  public function setApiKey($api_key) {
    $this->set('api_key', $api_key);
    return $this;
  }

  /**
   * Returns the entity secret key.
   *
   * @return string
   *   The entity secret key.
   */
  public function getSecretkey() {
    return $this->get('secret_key');
  }

  /**
   * Sets the entity secret key.
   *
   * @param string $secret_key
   *   Secret key.
   *
   * @return $this
   *   The Simplenews Mailjet Subscription entity.
   */
  public function setSecretkey($secret_key) {
    $this->set('secret_key', $secret_key);
    return $this;
  }

  /**
   * Sets the entity mapping table.
   *
   * @param string $mapping_table
   *   Mapping table.
   *
   * @return $this
   *   The Simplenews Mailjet Subscription entity.
   */
  public function setMappingTable($mapping_table) {
    $this->set('mapping_table', $mapping_table);
    return $this;
  }

  /**
   * Returns the entity mapping table.
   *
   * @return string
   *   The entity mapping table.
   */
  public function getMappingTable() {
    return $this->get('mapping_table');
  }

  /**
   * Show a message accordingly to status, after creating/updating an entity.
   *
   * @param int $status
   *   Status int, returned after creating/updating an entity.
   */
  public function statusMessage($status) {
    if ($status) {
      $this->messenger()->addMessage($this->t('Saved the %label entity.', ['%label' => $this->getTitle()]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label entity was not saved.', ['%label' => $this->getTitle()]));
    }
  }

}
