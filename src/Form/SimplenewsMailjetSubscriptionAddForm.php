<?php

namespace Drupal\simplenews_mailjet_subscriptions\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Form handler for the Simplenews Mailjet Subscription add and edit forms.
 */
class SimplenewsMailjetSubscriptionAddForm extends EntityForm {

  /**
   * Drupal\Core\Plugin\DefaultPluginManager definition.
   *
   * @var Drupal\Core\Entity\Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityQuery;

  /**
   * Class Constructor.
   */
  public function __construct(EntityTypeManagerInterface $entityQuery) {
    $this->entityQuery = $entityQuery;
  }

  /**
   * Class Create.
   *
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->getTitle(),
      '#help' => $this->t('Configuration title'),
      '#required' => TRUE,
      '#description' => $this->t('Name of the contact list.'),
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('List ID'),
      '#default_value' => $this->entity->id(),
      '#disabled' => !$this->entity->isNew(),
      '#description' => $this->t('Unique numeric ID assigned to this contact list.'),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
    ];
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#maxlength' => 32,
      '#description' => $this->t('Mailjet Account API Key'),
      '#required' => TRUE,
    ];
    $form['secret_key'] = [
      '#type' => 'password',
      '#title' => $this->t('Secret Key'),
      '#maxlength' => 32,
      '#description' => $this->t('Mailjet Account Secret Key'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $status = $this->entity->save();
    $this->entity->statusMessage($status);
    $form_state->setRedirect('entity.simplenews_mailjet_subscriptions.list');
  }

  /**
   * Check whether a Simplenews Mailjet Subscription entity exists.
   *
   * @param mixed $id
   *   Entity id.
   *
   * @return bool
   *   True if entity already exists
   */
  public function exist($id) {
    $query = $this->entityTypeManager->getStorage('simplenews_mailjet_subscription')->getQuery();
    $entity = $query->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}
