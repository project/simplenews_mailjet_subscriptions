<?php

namespace Drupal\simplenews_mailjet_subscriptions\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Form handler for the Simplenews Mailjet Subscriptions manage properties form.
 */
class SimplenewsMailjetSubscriptionEditForm extends EntityForm {

  const FORM_TABLE = 'mapping_table';

  const SIMPLENEWS_NEWSLETTER = 'simplenews_news';

  const CONTACT_PROPERTY = 'contact_property';

  const NEWSLETTER_PROPERTY_NAME = 'newsletter_property_name';

  const ADD_NEWSLETTER = 'add_newsletter';
  /**
   * Plugin field type.
   *
   * @var object
   */
  protected $pluginFieldType;

  /**
   * Entity type manager.
   *
   * @var object
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Plugin\DefaultPluginManager definition.
   *
   * @var Drupal\Core\Entity\Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityQuery;

  /**
   * Class Constructor.
   */
  public function __construct(EntityTypeManagerInterface $entityQuery) {
    $this->entityQuery = $entityQuery;
  }

  /**
   * Class Create.
   *
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      // Load the service required to construct this class.
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->getTitle(),
      '#help' => $this->t('Configuration title'),
      '#required' => TRUE,
    ];
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#maxlength' => 32,
      '#default_value' => $this->entity->getApiKey(),
      '#help' => $this->t('Mailjet Account API Key'),
      '#required' => TRUE,
      '#disabled' => !$this->entity->isNew(),
    ];

    if ($this->entity->getApiKey() && $this->entity->getSecretkey()) {
      $this->constructTable($form, $form_state);

    }

    $form['add_subscription'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add Row'),
      '#submit' => [[$this, 'addRow']],
    ];

    return $form;
  }

  /**
   * Build table with mapping between Newsletters and Mailjet Properties.
   *
   * @param array $form
   *   Form entity array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form State.
   */
  public function constructTable(array &$form, FormStateInterface $form_state) {

    $newsletters = $this->entityTypeManager->getStorage('simplenews_newsletter')->loadMultiple();
    // Table header.
    $header = [
      self::SIMPLENEWS_NEWSLETTER => $this->t('Simplenews Newsletter'),
      self::CONTACT_PROPERTY => $this->t('Contact Property'),
      self::NEWSLETTER_PROPERTY_NAME => $this->t('Newsletter Property Name'),
    ];

    $form[self::FORM_TABLE] = [
      '#type' => 'table',
      '#header' => $header,
    ];

    $newsletter_options[0] = $this->t('None');
    foreach ($newsletters as $id => $newsletter) {
      $newsletter_options[$id] = $newsletter->name;

    }
    $table_values = $this->entity->getMappingTable();
    $rows = count($table_values);

    for ($row = 0; $row <= $rows; $row++) {
      $newsletter_options[0] = $this->t('None');
      foreach ($newsletters as $id => $newsletter) {
        $newsletter_options[$id] = $newsletter->name;

      }
      // Checkboxes with content type fields.
      $form[self::FORM_TABLE][$row][self::SIMPLENEWS_NEWSLETTER] = [
        '#type' => 'select',
        '#options' => $newsletter_options,
        '#default_value' => $table_values[$row][self::SIMPLENEWS_NEWSLETTER],
      ];
      // Checkboxes with content type fields.
      $form[self::FORM_TABLE][$row][self::CONTACT_PROPERTY] = [
        '#type' => 'textfield',
        '#size' => 30,
        '#maxlength' => 255,
        '#default_value' => $table_values[$row][self::CONTACT_PROPERTY],
      ];
      // Checkboxes with content type fields.
      $form[self::FORM_TABLE][$row][self::NEWSLETTER_PROPERTY_NAME] = [
        '#type' => 'textfield',
        '#size' => 30,
        '#maxlength' => 255,
        '#default_value' => $table_values[$row][self::NEWSLETTER_PROPERTY_NAME],
      ];

    }

  }

  /**
   * Add Row.
   *
   * @param array $form
   *   Form entity array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form State.
   */
  public function addRow(array &$form, FormStateInterface $form_state) {
    $newsletter_options = '';
    $this->entity->save();
    $table_values = $this->entity->getMappingTable();
    $row = count($table_values);

    $form[self::FORM_TABLE][$row][self::SIMPLENEWS_NEWSLETTER] = [
      '#type' => 'select',
      '#options' => $newsletter_options,
    ];
    // Checkboxes with content type fields.
    $form[self::FORM_TABLE][$row][self::CONTACT_PROPERTY] = [
      '#type' => 'textfield',
      '#size' => 30,
      '#maxlength' => 255,

    ];
    // Checkboxes with content type fields.
    $form[self::FORM_TABLE][$row][self::NEWSLETTER_PROPERTY_NAME] = [
      '#type' => 'textfield',
      '#size' => 30,
      '#maxlength' => 255,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $table_values = $this->entity->getMappingTable();
    $rows = count($table_values);

    for ($row = 0; $row < $rows; $row++) {
      if ($table_values[$row][self::SIMPLENEWS_NEWSLETTER] == '0') {
        array_splice($table_values, $row, 1);
        $row--;
      }
    }
    $this->entity->setMappingTable($table_values);
    $status = $this->entity->save();
    $this->entity->statusMessage($status);
    $form_state->setRedirect('entity.simplenews_mailjet_subscriptions.list');
  }

  /**
   * Check whether a Simplenews Mailjet Subscription entity exists.
   *
   * @param mixed $id
   *   Entity id.
   *
   * @return bool
   *   True if entity already exists
   */
  public function exist($id) {
    $query = $this->entityTypeManager->getStorage('simplenews_mailjet_subscription')->getQuery();
    $entity = $query->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}
