<?php

namespace Drupal\simplenews_mailjet_subscriptions\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Provides a list of Simplenews Mailjet Subscription entities.
 */
class SimplenewsMailjetSubscriptionsListBuilder extends ConfigEntityListBuilder {

  const TITLE = 'title';

  /**
   * Constructs the table header.
   *
   * @return array
   *   Table header
   */
  public function buildHeader() {
    $header[self::TITLE] = $this->t('Title');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {

    $row[self::TITLE] = $entity->get('title') . ' (' . $entity->id() . ')';

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity, $type = 'edit') {
    $operations = parent::getDefaultOperations($entity);
    $operations['edit'] = [
      self::TITLE => $this->t('Edit'),
      'weight' => 0,
      'url' => Url::fromRoute('entity.simplenews_mailjet_subscriptions.edit_form', ['simplenews_mailjet_subscription' => $entity->id()]),
    ];
    $operations['delete'] = [
      self::TITLE => $this->t('Delete'),
      'weight' => 0,
      'url' => Url::fromRoute('entity.simplenews_mailjet_subscriptions.delete_form', ['simplenews_mailjet_subscription' => $entity->id()]),
    ];

    return $operations;
  }

}
